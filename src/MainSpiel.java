import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.text.Font;
import javafx.stage.Stage;
import model.Model;


public class MainSpiel extends Application {

    // Eigenschaften initialisieren
    private Timer timer;

    // Methoden
    @Override
    public void start(Stage stage) throws Exception {

        /* javaFX Vorbereitungen / Stage
        /////////////////////////////////*/

        // Canvas
        Canvas canvas = new Canvas(Model.WIDTH, Model.HEIGHT);


        // Group
        Group group = new Group();
        group.getChildren().add(canvas);

        //Scene
        Scene scene = new Scene(group);

        // Stage
        stage.setScene(scene);
        stage.setTitle("ReflexION");
        stage.show();

        // Draw
        GraphicsContext bg = canvas.getGraphicsContext2D();
        GraphicsContext gc = canvas.getGraphicsContext2D();
        GraphicsContext sc = canvas.getGraphicsContext2D();
        GraphicsContext tc = canvas.getGraphicsContext2D();
        tc.setFont(new Font("Arial", 60));

        Model model = new Model();
        Graphics graphics = new Graphics(model, gc, sc, tc, bg);
        timer = new Timer(model, graphics);
        timer.start();

        // InputHandler
        InputHandler inputHandler = new InputHandler(model);

        canvas.setOnMouseClicked(
                event -> inputHandler.onMouseClicked(
                        (int) event.getX(),
                        (int) event.getY()
                )
        );
        scene.setOnKeyPressed(
                event -> inputHandler.onKeyPressed(event.getCode())
        );
    }


    @Override
    public void stop() throws Exception {
        timer.stop();
        super.stop();
    }
}
