import javafx.scene.input.KeyCode;
import model.Model;


public class InputHandler {

    // Eigenschaften
    private Model model;

    // Konstruktoren
    public InputHandler(Model model) {
        this.model = model;
    }

    // Methoden

    //Tasten
    public void onKeyPressed(KeyCode key) {
        if (key == KeyCode.SPACE) {
            if (!model.isStartGame()) {
                model.setStartGame(true);
            }
        }
    }

    //Maus
    public void onMouseClicked(int x, int y) {
        model.scanTargets(x, y);
    }
}
