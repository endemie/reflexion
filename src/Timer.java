import javafx.animation.AnimationTimer;
import model.Model;

public class Timer extends AnimationTimer {

    // Eigenschaften
    private long previousTime = -1;
    public long nowSec = 0;
    private Model model;
    private Graphics graphics;

    // Kontruktoren
    public Timer(Model model, Graphics graphics) {
        this.model = model;
        this.graphics = graphics;
    }

    // Methoden
    @Override
    public void handle(long nowNano) {
        long nowMilli = nowNano / 1000000;
        long elapsedTime;
        if (previousTime == -1) {
            elapsedTime = 0;
        } else {
            elapsedTime = nowMilli - previousTime;
        }

        //Erstellung des Countdowns
        model.setCountdown((30 - ((nowSec += elapsedTime) / 1000)));

        //Counter wird zurückgesetzt, nachdem alle Ziele erledigt sind
        if (model.getTargets().isEmpty()) {
            nowSec = 0;
        }

        //Methoden, die geupdated werden müssen während das Spiel läuft
        if ((model.getCountdown() >= 0) && (model.isStartGame())) {
            previousTime = nowMilli;
            model.newLevel(model.getTargets());
            model.updatePosX(elapsedTime);
            model.updateBackground();
        }
        //zeichnen
        graphics.draw();
    }
}
