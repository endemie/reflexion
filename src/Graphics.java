
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;
import javafx.scene.paint.ImagePattern;
import javafx.scene.text.Font;
import model.Target;
import model.Model;
import model.Tunnel;

public class Graphics {

    // Eigenschaften
    private Model model;
    private GraphicsContext bg;
    private GraphicsContext gc;
    private GraphicsContext sc;
    private GraphicsContext tc;
    Image start = new Image("sprites/start.jpg");

    // Konstruktoren

    public Graphics(Model model, GraphicsContext gc, GraphicsContext sc, GraphicsContext tc, GraphicsContext bg) {
        this.model = model;
        this.bg = bg;
        this.gc = gc;
        this.sc = sc;
        this.tc = tc;
    }

    // Methoden
    public void draw() {

        //Zeichnen bei Spielstart
        if (model.isStartGame()) {
            // Clear Screen
            gc.clearRect(0, 0, Model.WIDTH, Model.HEIGHT);

            // import Graphics
            Image tunnelGraphic = new Image("Sprites/tunnel.png");
            Image targetGraphic = new Image("Sprites/target.png");

            // Draw Background
            DrawBackground(tunnelGraphic);

            //Draw Targets
            drawTargets(targetGraphic);

            // Draw Scorecounter
            DrawInfo(sc, "SCORE: ", model.getScore(), Model.WIDTH / 8, "GAME ", 3);

            // Draw Timer
            DrawInfo(tc, "TIMER: ", model.getCountdown(), (Model.WIDTH / 8) * 5, " OVER", 2);

            //Zeichnen vor Start
        } else {
            gc.setFill(new ImagePattern(start));
            gc.fillRect(0, 0, 1024, 768);
            tc.setFill(Color.GREENYELLOW);
            tc.fillText("Press \"SPACE\" to start ReflexION", (Model.WIDTH / 2) - 450, (Model.HEIGHT / 2) - 25);

        }

    }

    //Ziele zeichnen
    private void drawTargets(Image targetGraphic) {
        for (Target target : this.model.getTargets()) {
            gc.setFill(new ImagePattern(targetGraphic));
            gc.fillOval(
                    target.getX() - target.getW() / 2,
                    target.getY() - target.getH() / 2,
                    target.getW(),
                    target.getH()
            );

            // Paint Font
            sc.setFill(Color.LIGHTCORAL);
        }
    }

    //Infos zeichnen
    private void DrawInfo(GraphicsContext tc, String s, long countdown, int i, String s2, int i2) {
        if (model.getCountdown() > 0) {
            tc.fillText(s + countdown, i, 100);
            tc.setFont(new Font("Arial", 60));
        } else {
            tc.fillText(s2, (Model.WIDTH / i2), (Model.HEIGHT / 2) - 25);
        }
    }

    //Hintergrund zeichnen
    private void DrawBackground(Image tunnelGraphic) {
        for (Tunnel tunnel : model.getBgList()) {
            bg.setFill(new ImagePattern(tunnelGraphic));
            bg.fillRect(tunnel.getX() + 35, tunnel.getY(), tunnel.getH(), tunnel.getW());
        }
    }
}
