package model;

import model.util.TargetFactory;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class Model {

    // FINALS
    public static final int WIDTH = 1024;
    public static final int HEIGHT = 768;

    // Eigenschaften
    private List<Tunnel> bgList = new LinkedList<>();
    private Tunnel bg;
    private List<Target> targets = new LinkedList<>();
    private Player player;
    private Random r = new Random();
    private static int score = 0;
    private static int diff = 1;
    private Target hittedTarget;
    private static long countdown;
    private boolean startGame = false;

    // Konstruktoren
    public Model() {
        bg = new Tunnel(Model.WIDTH / 2 - 250, Model.HEIGHT / 2 - 250, 500, 500);
        bgList.add(bg);
        this.player = new Player(0, 0);
        newLevel(this.targets);
    }

    // Methoden

    //X-Position updaten
    public void updatePosX(long elapsedTime) {
        for (Target target : targets) {
            target.updateX(elapsedTime);
        }
    }

    //Hintergrund updaten
    public void updateBackground() {
        if (bg.getW() >= WIDTH / 32) {
            bg = new Tunnel(Model.WIDTH / 2 - 40, Model.HEIGHT / 2 - 40, 5, 5);
            bgList.add(bg);
        }
        for (Tunnel tunnel : bgList) {
            tunnel.update();
        }
    }

    // neue Ziele erschaffen, wenn alle abgeschossen

    public void newLevel(List<Target> list) {
        if (list.isEmpty()) {
            for (int i = 0; i < 6; i++) {
                this.targets.add(TargetFactory.createTarget());
            }
            Model.setDiff(Model.getDiff() + 1);
        }
    }

    // Abfrage, ob Ziel getroffen, dann löschen

    private void eliminateTargets(int mouseX, int mouseY, Target t) {
        int leftBorderOfTarget = t.getX() - (t.getW() / 2);
        int rightBorderOfTarget = t.getX() + (t.getW() / 2);
        int upperBorderOfTarget = t.getY() - (t.getH() / 2);
        int lowerBorderOfTarget = t.getY() + (t.getH() / 2);

        boolean matchX = (mouseX >= leftBorderOfTarget) && (mouseX <= rightBorderOfTarget);
        boolean matchY = (mouseY >= upperBorderOfTarget) && (mouseY <= lowerBorderOfTarget);

        if (matchX && matchY) {
            score += 10 * (Model.getDiff() - 1);
            this.hittedTarget = t;
        }
    }

    //Abfrage aller Ziele

    public void scanTargets(int mouseX, int mouseY) {
        for (Target t : targets) {
            eliminateTargets(mouseX, mouseY, t);
        }
        targets.remove(this.hittedTarget);
        this.hittedTarget = null;
    }

    // Setter + Getter
    public List<Target> getTargets() {
        return targets;
    }

    public Player getPlayer() {
        return player;
    }

    public static int getScore() {
        return score;
    }

    public static void setScore(int score) {
        Model.score = score;
    }

    public static int getDiff() {
        return diff;
    }

    public static void setDiff(int diff) {
        Model.diff = diff;
    }

    public List<Tunnel> getBgList() {
        return bgList;
    }

    public void setCountdown(long countdown) {
        this.countdown = countdown;
    }

    public long getCountdown() {
        return countdown;
    }

    public boolean isStartGame() {
        return startGame;
    }

    public void setStartGame(boolean startGame) {
        this.startGame = startGame;
    }

}
