package model;

public class Target {

    // Eigenschaften
    private int x;
    private int y;
    private int h;
    private int w;
    private float speedX;
    private boolean onLeftBorder;
    private boolean onUpperBorder;

    // Konstruktoren
    public Target(int x, int y, int h, float speedX) {
        this.x = x;
        this.y = y;
        this.h = h;
        this.w = h;
        this.speedX = speedX;
        this.onLeftBorder = true;
        this.onUpperBorder = true;
    }

    // Methoden

    //Bewegungsmuster auf X-Achse
    public void updateX(long elapsedTime) {
        if (this.x <= 0 + (this.w) / 2) {
            this.onLeftBorder = true;
        }
        if (this.x >= (1024 - (this.w) / 2)) {
            this.onLeftBorder = false;
        }
        if (this.onLeftBorder) {
            this.x = Math.round(this.x + elapsedTime * speedX);
        } else {
            this.x = Math.round(this.x - elapsedTime * speedX);
        }
    }

    //Codeleiche: Bewegungsmuster für Y-Achse (für später mal)
    /*
    public void updateY(long elapsedTime) {
        if (this.y <= 0 + ((this.w) / 2)) {
            this.onUpperBorder = true;
        }
        if (this.y >= 768 - ((this.w) / 2)) {
            this.onUpperBorder = false;
        }
        if (this.onUpperBorder) {
            this.y = Math.round(this.y + elapsedTime * speedX);
        } else {
            this.y = Math.round(this.y - elapsedTime * speedX);
        }
    }
    */

    //Getter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }

    public float getSpeedX() {
        return speedX;
    }
}
