package model;

public class Tunnel {

    private int x, y, h, w;
    private float speed;

    public Tunnel(int x, int y, int h, int w) {
        this.x = x;
        this.y = y;
        this.h = h;
        this.w = w;
    }

    //Update Background
    public void update() {
        speed = h * 0.35f;
        x -= speed / 2;
        y -= speed / 2;
        h += speed;
        w += speed;
    }

    //Getter
    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getH() {
        return h;
    }

    public int getW() {
        return w;
    }
}
