package model.util;

import model.Model;
import model.Target;

import java.util.Random;

public class TargetFactory {

    public static Target createTarget() {
        Random r = new Random();
        Target t = new Target(r.nextInt(900), r.nextInt(500) + 200, 110 - (Model.getDiff() * 3), 0.1f * Model.getDiff());
        return t;
    }

}
